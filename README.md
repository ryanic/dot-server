# You'll need to set this up on a separate VM to capture the traffic during the demo. I used Kali, but any Debian should work with the following config.

# Install stubby
```apt-get install stubby -y```

# Edit stubby.conf (change address_data to VMWare host's VMNet IP)
```
cat > /etc/stubby/stubby.conf << EOF
resolution_type: GETDNS_RESOLUTION_STUB
dns_transport_list:
  - GETDNS_TRANSPORT_TLS
tls_authentication: GETDNS_AUTHENTICATION_REQUIRED
tls_query_padding_blocksize: 128
edns_client_subnet_private : 1
round_robin_upstreams: 1
idle_timeout: 10000
listen_addresses:
  - 127.0.0.1
  - 0::1
upstream_recursive_servers:
  - address_data: 192.168.14.1
    tls_port: 853
    tls_auth_name: "dot-server"
    tls_pubkey_pinset:
      - digest: "sha256"
        value: 5pcx2nuP9z9vV0RjtH7ACdLraoAX7Z0BID04Nzu7jAU=
EOF
```
# Enable/start stubby
```systemctl enable stubby```

```systemctl start stubby```

# Show a before (DNS in clear text)
```nslookup www.google.com```

<img src="https://gitlab.com/ryanic/dot-server/raw/master/images/before.png" width="70%"/>

# Change DNS setting in VM to 127.0.0.1 (may need to toggle interface on/off to take affect)

# Doing another nslookup should send DNS over TLS (TCP 853)
```nslookup sec530.com```

<img src="https://gitlab.com/ryanic/dot-server/raw/master/images/after.png" width="70%"/>
