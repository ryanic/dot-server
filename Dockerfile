FROM nginx:latest
COPY nginx.conf /etc/nginx/nginx.conf
COPY dot-server.key /etc/nginx/dot-server.key
COPY dot-server.crt /etc/nginx/dot-server.crt
RUN chmod 400 /etc/nginx/dot-server.key
EXPOSE 853
