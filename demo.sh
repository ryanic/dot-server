#!/bin/bash

# Check if Docker image is created
if ! [[ $(docker images | grep dot-server) ]]; then
    echo -e "\033[95mBuilding Docker image...\033[0m"
    docker build -t dot-server .
fi

echo -e "\033[95mStarting Docker container...\033[0m"
docker run -it --rm -d -p 853:853 --name dot-server dot-server
echo -e "\033[95mOpen Kali VM and set DNS to 127.0.0.1...\033[0m"
echo -en "\033[95mPress [Enter] key to stop demo...\033[0m"
read -p ""
echo -e "\033[95mStopping Docker container...\033[0m"
docker stop dot-server
